
	SoftwareBySlim Website Rebuild

 I wanted my personal website, which was the first site I built, to have
cleaner, more user friendly experience. 

 From the lighter background, to flat logos and buttons, I think the 
rebuild offers a better representation of my company, and how I want to
represent it to the public.

 I am always looking for feedback, so feel free to contact me on twitter
at twitter.com/softwarebyslim, or via email at slim@softwarebyslim.ca.  
