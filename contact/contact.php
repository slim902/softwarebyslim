<?PHP
/*
    Contact Form from HTML Form Guide
	This program is free software published under the
	terms of the GNU Lesser General Public License.
	See this page for more info:
	http://www.html-form-guide.com/contact-form/creating-a-contact-form.html
*/
require_once("./include/fgcontactform.php");

$formproc = new FGContactForm();


//1. Add your email address here.
//You can add more than one receipients.
$formproc->AddRecipient('slim@softwarebyslim.ca'); //<<---Put your email address here


//2. For better security. Get a random tring from this link: http://tinyurl.com/randstr
// and put it here
$formproc->SetFormRandomKey('mXd11wqNuflQFBN');


if(isset($_POST['submitted']))
{
	if($formproc->ProcessForm())
	{
		$formproc->RedirectToURL("thank-you.php");
	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>SBS:Contact</title>

	<meta name="keywords" content="softwarebyslim, software, website, web development, slim, slim902, tech, support, technical, apps" />
	<meta name="description" content="Software Development, Web Design/Development and Technial Support" />

	<link rel="icon" type="image/png" href="../images/favicon.png">
	
	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Source+Code+Pro' rel='stylesheet' type='text/css'>
	
	<link rel="stylesheet" type="text/css" href="../css/sbs.css" media="screen"  title="default"/>
    <link rel="stylesheet" type="text/css" href="../css/logoCss.css" />
    <link rel="stylesheet" type="text/css" href="contact.css" />
    <link rel="stylesheet" type="text/css" href="../css/mediaQueries.css" media="screen" />

    <script type="text/javascript"> 
      
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-34244982-1']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
        <!-- Google Analytics -->
	
</head>
<body>

	<div class = "headerDiv">

		<div class='textLogo'>
			<span class='logoSpan1'>></span><span class='logoSpan2'>S</span><span class='logoSpan3'>o</span><span class='logoSpan4'>f</span><span class='logoSpan5'>t</span><span class='logoSpan6'>w</span><span class='logoSpan7'>a</span><span class='logoSpan8'>re</span><span class='logoSpace'> </span><span class='logoSpan9'>By</span><span class='logoSpace'> </span><span class='logoSpan10'>S</span><span class='logoSpan12'>l</span><span class='logoSpan13'>i</span><span class='logoSpan14'>m</span><span class='logoSpan15'>_</span>
		</div>	
        
		<div class='homeButtonDiv'>
			<img class="homeButton" src="../images/homeIcon.png"></img>
		</div> 

			<!--div class="logoDiv"-->

            <!-- img class='siteTitleLogo' id="logo" src="images/newSBSlogo.png" alt="Logo"></img-->  <!-- width="210" height="110"-->
            <!--img class='siteTitleLogo' id="smallLogo" src="../images/newSBSlogoMED.png" alt="Logo"></img-->   <!--width="150" height="79"-->
            <!-- img class='siteTitleLogo' id="smallSmallLogo" src="images/sbsLogoSmallSmall.png" alt="Logo"></img-->   <!--width="100" height="52"--> 
            
        <!--/div-->
	</div>
	
	<div class = "content">
		
		<div class ="textDiv" id= "contact">
			<h2 class="textDivHeader">Contact Slim</h2>

            <p class="textDivP">Would like to obtain a quote, have a question about my services, or any other inquiry you might have you can reach me in any of the following ways:</p>
			<table id="table_contact_info" align="center" >
				<tr class='phone'><td>Phone: (902) 488-2265</td></tr>
				<tr class='email'><td>Email: slim@softwarebyslim.ca</td></tr>
				<tr class='twitter'><td><a href="https://twitter.com/softwarebyslim" id="twitterFollowButton" class="twitter-follow-button" data-show-count="false" data-dnt="true">Follow @softwarebyslim</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></td></tr>
			</table>
			<p class="textDivP">You can also send us a brief description of what you are inquiring about using the form below, and we will contact you directly.</p>
	        <div class="contactForm">
                <!-- Form Code Start -->
                <form id='contactus' action='<?php echo $formproc->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
                <fieldset >
                <legend>Contact Slim</legend>

                <input type='hidden' name='submitted' id='submitted' value='1'/>
                <input type='hidden' name='<?php echo $formproc->GetFormIDInputName(); ?>' value='<?php echo $formproc->GetFormIDInputValue(); ?>'/>
                <input type='text'  class='spmhidip' name='<?php echo $formproc->GetSpamTrapInputName(); ?>' />

                <div class='short_explanation'>* required fields</div>

                <div><span class='error'><?php echo $formproc->GetErrorMessage(); ?></span></div>
                <div class='container'>
                    <label for='name' >Your Full Name*: </label><br/>
                    <input type='text' name='name' id='name' value='<?php echo $formproc->SafeDisplay('name') ?>' maxlength="50" /><br/>
                    <span id='contactus_name_errorloc' class='error'></span>
                </div>
                <div class='container'>
                    <label for='email' >Email Address*:</label><br/>
                    <input type='text' name='email' id='email' value='<?php echo $formproc->SafeDisplay('email') ?>' maxlength="50" /><br/>
                    <span id='contactus_email_errorloc' class='error'></span>
                </div>

                <div class='container'>
                    <label for='phone' >Phone Number:</label><br/>
                    <input type='text' name='phone' id='phone' value='<?php echo $formproc->SafeDisplay('phone') ?>' maxlength="50" /><br/>
                    <span id='contactus_email_errorloc' class='error'></span>
                </div>

                <div class='container'>
                    <label for='message' >Message:</label><br/>
                    <span id='contactus_message_errorloc' class='error'></span>
                    <textarea rows="10" cols="50" name='message' id='message'><?php echo $formproc->SafeDisplay('message') ?></textarea>
                </div>


                <div class='container'>
                    <input type='submit' name='Submit' value='Submit' />
                </div>

                </fieldset>
                </form>
                <!-- client-side Form Validations:
                Uses the excellent form validation script from JavaScript-coder.com-->

                <script type='text/javascript'>
                // <![CDATA[

                    var frmvalidator  = new Validator("contactus");
                    frmvalidator.EnableOnPageErrorDisplay();
                    frmvalidator.EnableMsgsTogether();
                    frmvalidator.addValidation("name","req","Please provide your name");

                    frmvalidator.addValidation("email","req","Please provide your email address");

                    frmvalidator.addValidation("email","email","Please provide a valid email address");

                    frmvalidator.addValidation("message","maxlen=2048","The message is too long!(more than 2KB!)");

                // ]]>
                </script>
            </div>            
		</div>
	<div class = "footer">
		<div class = "navDiv" id="contactNavDiv">
			<ul class = "navBar">
				<li id="l1"><a id="homeNavButton" href="../home.html">Home</a></li>
				<li id="l2"><a id="codeNavButton" href="../code.html">Code</a></li>
				<li id="l3"><a id="designNavButton" href="../design.html">Design</a></li>
				<li id="l4"><a id="aboutNavButton" href="../about.html">About</a></li>
			</ul>
		</div
	</div>
</body>
</html>
