<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0">

	<meta name="keywords" content="softwarebyslim, software, slim, slim902, website,tech, support, technical, apps" />
    <meta name="description" content="Software Development, Web Design/Development and Technial Support" />

    <title>SBS: Contact</title>

	<meta name="keywords" content="softwarebyslim, software, website, web development, slim, slim902, tech, support, technical, apps" />
    <meta name="description" content="Software Development, Web Design/Development and Technial Support" />

    <link href='http://fonts.googleapis.com/css?family=Life+Savers|Happy+Monkey' rel='stylesheet' type='text/css'>      
        <!-- Google Fonts -->
        
    <link rel="icon" type="image/png" href="../images/favicon.png">

    <link rel="stylesheet" type="text/css" href="../css/sbs_wide.css" media="screen"  title="default"/>
    <link rel="stylesheet" type="text/css" href="contact.css" />
    <link rel="stylesheet" type="text/css" href="../css/mediaQueries.css" media="screen" />

     <script type='text/javascript' src='scripts/gen_validatorv31.js'></script>    
    <script type="text/javascript"> 
      
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-34244982-1']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
        <!-- Google Analytics -->
        
</head>

<body>


    <div class="header_div">

        <div class="logoDiv">

            <img class='siteTitleLogo' id="logo" src="../images/sbsLogo.png" alt="Logo" width="210" height="110"></img>
            <img class='siteTitleLogo' id="smallLogo" src="../images/sbsLogoSmall.png" alt="Logo" width="150" height="79" ></img>
            <img class='siteTitleLogo' id="smallSmallLogo" src="../images/sbsLogoSmallSmall.png" alt="Logo" width="100" height="52" ></img>            
            <span class='logoDivSpan'>Software By Slim</span>
            
        </div>

        <div class="navDiv">

            <ul class='navBar'>
                <li><a class="aFirstChild" href="index.html">Home</a></li>
                <li class="even"><a href="about.html">About Me</a></li>
                <li class="odd"><a href="services.html">Services</a></li>
                <li class="bottomRow" class="even" ><a href="mywork.html">My Work</a></li>
                <li class="bottomRow"><a class="aLastChild" href="contact.html">Contact</a></li>
            </ul>
        
        </div>

    </div>       

    <div class="contentDiv">

        <div class="content">

            <h2 class="main_content_title">Thanks for contacting us!</h2>

                <p class="main_content">Thank you for contacting Software By Slim, I will be in contact as soon as possible.</p>

        </div>

    </div>

    <div class="footer_div">

    </div>
</body>
</html>
   
